module.exports = {
    query: {
        D_City: 'OSL',
        A_City: 'RIX',
        TripType: '1',
        AduldCount: '1',
        ChildCount: '0',
        InfantCount: '0',
        D_Day: '01',
        D_Month: '201712',
        R_Day: '01',
        R_Month: '201712',
        CabinFareType: '2',
        IncludeTransit: 'false',
        AgreementCodeFK: '-1',
        CurrencyCode: 'EUR'
    }
}