const fetch = require('node-fetch');
const cheerio = require('cheerio');
const { URL, URLSearchParams } = require('url');
const searchParameters = require('./searchParameters');
const fs = require('fs');

// const debug = require('debug')('fetch')

let parameters = new URLSearchParams(searchParameters.query);

//We get all flight dates from table
let departuresTableURL = new URL ('https://www.norwegian.com/en/booking/flight-tickets/farecalendar/');
let selectFlightURL = new URL('https://www.norwegian.com/en/ipc/availability/avaday');
let flightDates = [];

departuresTableURL.search = parameters;
selectFlightURL.search = parameters;

fetch(departuresTableURL.href)
    .then((res) => res.text())
    .then((body) => {
        let $ = cheerio.load(body);
        $('div[class^=fareCalDayDirect]')
            .find('.fareCalDate')
            .each((index, element) => {
                let flightDate = $(element).text().trim();
                flightDates.push(flightDate);
            });
    })
    .then(() => {
        flightDates.forEach((date) => {
            (function(date) {
                let newSearch = selectFlightURL;
                newSearch.searchParams.set('D_Day', date);

                //NB: dFlight=undefined in query string means, that 'Flex' fare is set by default, which in turn shows taxes panel.
                newSearch.searchParams.set('dFlight', undefined);

                scrapeData(newSearch.href, date);
            })(date);
        });
    })
    .catch((err) => {
        console.error(Error('Error in promise chain ' + err));
    });



function scrapeData (url, date) {
    fetch(url)
        .then((res) => res.text())
        .then((body) => {
            let $ = cheerio.load(body);

            let lowestPrice = $('.fareselect.standardlowfare').find('label').text().trim() || 'No tickets available.';
            let departureTime = $('td.depdest > div.content.emphasize').text().trim();
            let arrivalTime = $('td.arrdest > div.content.emphasize').text().trim();
            let taxes = $('#ctl00_MainContent_ipcAvaDay_upnlResSelection > div:nth-child(1) > div > table > tbody > tr:nth-child(15) > td.rightcell.emphasize').text().trim();

            let flight = {
                date: date,
                departure_airport: searchParameters.query.D_City,
                arrival_airport: searchParameters.query.A_City,
                connection_airport: 'Direct flight',
                departure_time: departureTime,
                arrival_time: arrivalTime,
                cheapest_price: lowestPrice,
                taxes: taxes
            };
 
            let resultsFile = `${searchParameters.query.D_City} - ${searchParameters.query.A_City} - ${searchParameters.query.D_Month}`;
            
            fs.appendFile(`./results/${resultsFile}.json`, (JSON.stringify(flight) + ',\n'), function(err) {
                if (err) console.error(Error('Error while writing results: ' + err));
                console.log(flight);
            });
        });
};
