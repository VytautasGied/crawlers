const cheerio = require('cheerio');
const searchParameters = require('./searchParameters');

exports.fillFirst = function(responseBody) {
    let $ = cheerio.load(responseBody);
    let form = {};
    $('input').each((index, element) => {
        let key = $(element).attr('name');
        let value = $(element).attr('value') || '';
        if (key !== undefined && key !== 'ctl00$FullRegion$TopRegion$_siteHeader$_ssoLogin$MainFormBorderPanel$url') {
            form[key] = value;
        }
    });


    form.__EVENTTARGET = 'ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$Searchbtn$ButtonLink';
    form.            ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$ceptravelTypeSelector$TripTypeSelector = searchParameters.travelType;

    form.ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$predictiveSearch$txtFrom = 'Stockholm, Sweden - Arlanda (ARN)';

    form.ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$predictiveSearch$hiddenFrom = searchParameters.departureAirport;

    form.ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$predictiveSearch$txtTo = 'London, United Kingdom - Heathrow (LHR)';

    form.ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$predictiveSearch$hiddenTo = searchParameters.arrivalAirport;

    form.ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepCalendar$hiddenOutbound = searchParameters.departureDate;

    form.ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepCalendar$hiddenReturn = searchParameters.returnDate;

    form.ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepCalendar$hiddenStoreCalDates = `${new Date(2017,10,17)},${new Date(2017,10,17)}, ${new Date(2018,10,11)}`;
    form.ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepCalendar$selectOutbound = searchParameters.departureDate;

    form.ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepCalendar$selectReturn = searchParameters.returnDate;

    form.ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepPassengerTypes$passengerTypeAdult = searchParameters.adultPassengers;

    form.ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepPassengerTypes$passengerTypeChild211 = searchParameters.childPassengers;

    form.ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepPassengerTypes$passengerTypeInfant = searchParameters.infantPassengers;

    return form;

};

exports.fillSecond = function(responseBody) {
    let $ = cheerio.load(responseBody);
    let form = {};
    $('input').each((index, element) => {
        let key = $(element).attr('name');
        let value = $(element).attr('value');

       if ( key !== 'btnSubmitAmadeus' && key !== 'btnSubmitSIP') {
          form[key] = value;
       };
    });
    form.__EVENTTARGET = 'btnSubmitAmadeus';
    // form.SO_SITE_QUEUE_OFFICE_ID = 'LONSK08RV';
    // form.SO_SITE_OFFICE_ID = 'LONSK08RV';
    // form.SO_SITE_ETKT_Q_OFFICE_ID = 'LONSK08RV';
    // form.WDS_FO_IATA = '91491411';
    return form;
};