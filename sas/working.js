const cookies = {
    // D_IID:'257CC6A7-012A-35B0-BEF2-9A87FAF8713C',
    // D_UID:'16608E0E-9F73-3033-A3D1-E5ADB88DA9BA',
    // D_ZID: 'C5FE234D-24B8-3A1A-A69B-7F97B4DCDF0A',
    // D_ZUID: '7F34AE36-D343-30A8-ADEB-E32292BAD6D0', 
    // D_HID: 'F67B3EA0-D05A-3BBD-9CA7-AAAEEA1F48F9',
    // // D_SID:'78.56.118.215:dKH7gEiSH5ZdcxNdynB5D9LrElAabOHorH5lAA82hHE',
    _uetsid: '_uet987edf20',
    _vwo_uuid_v2: '5292F2BE165EFE493225009C6AA6A103|757a45290ce1ef502f427b96a5cde8fd6'
};

const headers = {
    'Host': 'book.flysas.com',
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:56.0) Gecko/20100101 Firefox/56.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Accept-Encoding': 'deflate, br',
    'Referer': 'https://www.flysas.com/en/uk/',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Pragma': 'no-cache',
    'Cache-Control': 'no-cache'
}

module.exports = {cookies, headers};