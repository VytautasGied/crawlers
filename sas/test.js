const cheerio = require('cheerio');
const needle = require('needle');
const request = require('request');
const {fillFirst, fillSecond} = require('./forms');
let working = require('./working');

const mainPage = 'https://www.flysas.com/en/';
const targetUrl = 'https://book.flysas.com/pl/SAS/wds/Override.action?SO_SITE_EXT_PSPURL=https://classic.sas.dk/SASCredits/SASCreditsPaymentMaster.aspx&SO_SITE_TP_TPC_POST_EOT_WT=50000&SO_SITE_USE_ACK_URL_SERVICE=TRUE&WDS_URL_JSON_POINTS=ebwsprod.flysas.com%2FEAJI%2FEAJIService.aspx&SO_SITE_EBMS_API_SERVERURL=https%3A%2F%2F1aebwsprod.flysas.com%2FEBMSPointsInternal%2FEBMSPoints.asmx&WDS_SERVICING_FLOW_TE_SEATMAP=TRUE&WDS_SERVICING_FLOW_TE_XBAG=TRUE&WDS_SERVICING_FLOW_TE_MEAL=TRU';

let needleOptions = {
    follow: 10,
    multipart: true,
    compressed: true,
    follow_set_cookies: true,
    follow_keep_method: true,
    user_agent: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/62.0.3202.89 Chrome/62.0.3202.89 Safari/537.36'
};
(function scrape() {
    needle.get(mainPage, function(err, res) {
        if(err) console.error(Error('Error in GET req:', err));
        // needleOptions.multipart = true;
        let form = fillFirst(res.body);
        console.log(res.headers);
        needleOptions.cookies = res.cookies;
        
        needle.post(mainPage, form, needleOptions, function(err, res) {
            if(err) console.error(Error('Error in POST req:', err));
            console.log(res.headers);
            let secondForm = fillSecond(res.body);
            console.log(secondForm);
            
            needleOptions.headers = {
                'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Accept-Encoding':'gzip, deflate, br',
                'Accept-Language':'en-US,en;q=0.9,lt;q=0.8',
                'Cache-Control':'max-age=0',
                'Connection':'keep-alive',
                // 'Content-Length':'7402',
                'Content-Type':'application/x-www-form-urlencoded',
                'Host':'book.flysas.com',
                'Origin':'https://www.flysas.com',
                'Referer':'https://www.flysas.com/en/uk/',
                'Upgrade-Insecure-Requests':'1',
                'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/62.0.3202.89 Chrome/62.0.3202.89 Safari/537.36'
            };
            needleOptions.multipart = false;
            // needle.post(targetUrl, secondForm, needleOptions, function (err, res) {
            //     if (err) console.error(Error('Error in second POST req:', err));
    
            //     console.log(res.headers);
            //     console.log(res.body);
            // });
        });
    });
})();