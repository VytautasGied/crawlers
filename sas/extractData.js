const data = require('./dummyData');
const searchParameters = require('./searchParameters');

//First one cuts data into recommendations like in SAS page
//Others are used to dig out data
const re = /var recommendation \= new REV_AVDRecommendation([\s\S]*?)theREV_AVD_COForm.addRecommendation\(recommendation,([\s\S]*?)\);/g;
const flightRe = /segment\./g;
const pushRe = /recommendation\.segments\.push/g;
const dataRe = /\.([\s\S]*?)\"([\s\S]*?)\"/g;
const fareFamilyRe = /fareFamily/;

const recArray = data[0].match(re);
//Result arrays
const outbound = [];
const inbound = [];


function extractData (recArray) {
    recArray.forEach(item => {
        prepareData(item);
    });
};

/**
 * 
 * @param {string} recArrayItem a string from data array, before splitting into smaller strings
 */
function prepareData (recArrayItem) {
        const dataArray = recArrayItem.split(';');
        //First item contains constructort, last one is an empy string
        dataArray.shift();
        dataArray.pop();
        createRecommendation(dataArray);
};
/**
 * 
 * @param {array} dataArray an array of strings from recArrayItem
 */
function createRecommendation (dataArray) {
    let recommendation = [];
    //pop last item which contains price and flight direction
    const last = dataArray.pop();
    const priceAndTaxes = last.match(/'price':([\s\S]*?)' /)[0].split(',');
    const price = priceAndTaxes[0].split(':')[1].slice(1, -1);
    const taxes = priceAndTaxes[1].split(':')[1].slice(1, -1);
    
    //Get direction 0 = outbound, 1 = inbound
    const args = last.match(/\(recommendation\,([\s\S]*?)\)/)[0].split(',');
    const direction = args[1].slice(2, -1);
    const recommendationId = args[2].slice(1, -1);

    const fareFamily = dataArray[dataArray.length-1].match(/'([\s\S]*?)'/)[0].slice(1, -1);
    


    let flight = {};

    dataArray.forEach((string) => {
        if (string.match(flightRe)) {
            const propertyAndData = string.match(dataRe)[0];
            const property = propertyAndData
                .match(/.([\s\S]*?)=/)[0]
                .slice(1, -2);
            const data = propertyAndData
                .match(/"([\s\S]*?)"/)[0]
                .slice(1, -1);
            
            
            if ( property !== 'airlineCode' ) {
                
                //Filter and rename properties
                switch (property) {
                    case 'bLocation':
                        flight.departureAirport = data;
                        break;
                    case 'eLocation':
                        flight.arrivalAirport = data;
                        break;
                    case 'bDate':
                        flight.departureTime = data;
                        break;
                    case 'eDate':
                        flight.arrivalTime = data;  break;                  
                    default:
                        flight.flightId = data;
                };
            };
        } else if (string.match(pushRe)) {
            flight.fareFamily = fareFamily;
            flight.price = parseFloat(price);
            flight.taxes = parseFloat(taxes);
            recommendation.push(flight);
            flight = {};
        };
    });
    recommendation = handleConnectionFlights(recommendation);

    direction == '0'
        ? outbound.push( recommendation )
        : inbound.push( recommendation );
};

function handleConnectionFlights (flights) {
    //Remove unwanted connection airports
    const filtered = [];
    let matchArr = new RegExp(searchParameters.arrivalAirport);
    let matchDep = new RegExp(searchParameters.departureAirport);
    let matchCon = new RegExp(searchParameters.connectionAirport);
    flights.forEach((flight) => {
        if ((
            flight.departureAirport.match(matchArr) || flight.departureAirport.match(matchDep) ||flight.departureAirport.match(matchCon)
        ) && (
            flight.arrivalAirport.match(matchArr) || flight.arrivalAirport.match(matchDep) ||flight.arrivalAirport.match(matchCon)
        )) {
            filtered.push(flight);
        }
    });

    //Flights to and from connection airpors are separate objects. TODO merge them
    return filtered;
};

/**
 * 
 * @param {array} outbound 
 * @param {array} inbound 
 * @param {string} filter which class to display
 *  Classes cheapest -> most expensive: ECONBG, ECOA, PREMN, PREMB
 */
function displayData (outbound, inbound, filter) {
    displayOut = [];
    displayIn = [];
    console.log(outbound);

    outbound.forEach((item) => {
        if (item.fareFamily === filter) {
            displayOut.push(item);
        }
    });
    inbound.forEach((item) => {
        if (item.fareFamily === filter) {
            displayIn.push(item);
        }
    });

    console.log('CHEAPEST FLIGHTS: \n', outbound);
    console.log('CHEAPEST RETURN FLIGHTS: \n', inbound);
};

extractData(recArray);
displayData(outbound, inbound, 'ECONBG');
module.exports = extractData;