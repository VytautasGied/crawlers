module.exports = {
    departureAirport: 'ARN',
    arrivalAirport: 'LHR',
    connectionAirport: 'OSL', //OSL = any airport in oslo, osl+ = Gardermoen
    departureTime: '',
    arrivalTime: '',
    departureDate: '2017-12-04',
    travelType: 'roundtrip', //'oneway' or 'roundtrip'
    returnTate: '2017-12-10',
    returnTepartureTime: '',
    returnArrivalTime: '',
    adultPassengers: '1',
    childPassengers: '0',
    infantPassengers: '0'
}