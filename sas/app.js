const cheerio = require('cheerio');
const needle = require('needle');
const request = require('request');
const {fillFirst, fillSecond} = require('./forms');
let working = require('./working');

const mainPage = 'https://www.flysas.com/en/';
const targetUrl = 'https://book.flysas.com/pl/SAS/wds/Override.action?SO_SITE_EXT_PSPURL=https://classic.sas.dk/SASCredits/SASCreditsPaymentMaster.aspx&SO_SITE_TP_TPC_POST_EOT_WT=50000&SO_SITE_USE_ACK_URL_SERVICE=TRUE&WDS_URL_JSON_POINTS=ebwsprod.flysas.com%2FEAJI%2FEAJIService.aspx&SO_SITE_EBMS_API_SERVERURL=https%3A%2F%2F1aebwsprod.flysas.com%2FEBMSPointsInternal%2FEBMSPoints.asmx&WDS_SERVICING_FLOW_TE_SEATMAP=TRUE&WDS_SERVICING_FLOW_TE_XBAG=TRUE&WDS_SERVICING_FLOW_TE_MEAL=TRUE'

let needleOptions = {
    follow: 10,
    compressed: true,
    follow_set_cookies: true,
    follow_keep_method: true,
    user_agent: 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0'
};
(function scrape() {
    needle.get(mainPage, function(err, res) {
        if(err) console.error(Error('Error in GET req:', err));
        needleOptions.multipart = true;
        let form = fillFirst(res.body);
        console.log(res.headers);
        needleOptions.cookies = res.cookies;
        
        needle.post(mainPage, form, needleOptions, function(err, res) {
            if(err) console.error(Error('Error in POST req:', err));
            console.log(res.headers);
            let secondForm = fillSecond(res.body);
            var j = request.jar();
            Object.keys(working.cookies).forEach(function(element) {
                let cookie = request.cookie(`${element}=${working.cookies[element]}`);
                j.setCookie(cookie, '/');
            });
            Object.keys(needleOptions.cookies).forEach( function(element){
                let cookie = request.cookie(`${element}=${needleOptions.cookies[element]}`);
                j.setCookie(cookie, '/');
            });

            let requestOptions = {
                url: targetUrl,
                form: secondForm,
                jar: j,
                encoding: 'utf8',
                headers: {
                    'Host': 'book.flysas.com',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'Accept-Language': 'en-US,en;q=0.5',
                    'Accept-Encoding': 'deflate, br',
                    'Referer': 'https://www.flysas.com/en/uk/',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Connection': 'keep-alive',
                    'Origin': 'https://www.flysas.com',
                    'Upgrade-Insecure-Requests': '1',
                    'Pragma': 'no-cache',
                    'Cache-Control': 'max-age=0'
                }
            };
            request.post(requestOptions, function(err, res, body) {
                if (err) console.error('Error in third part', err);
                console.log(res.headers);
                /**
                 * @param {string} body takes in a string containing list of all prices and flighttimes,
                 *
                 */
                // const re = /this\.currentPoints([\s\S]*?)this\.pageTicket/g;
                // const dataWithNewlines = body.match(re);
                // const data = dataWithNewlines[0].replace(/\s{2,}/g,' ');
                // extractData(body);      
                console.log(body);
            });
        });
    });
})();